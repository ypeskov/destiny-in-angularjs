angular.module('templates-app', ['divination/i18n/en-us/templates/divination-description.tpl.html', 'divination/i18n/en-us/templates/divination-list.tpl.html', 'divination/i18n/en-us/templates/divination-result.tpl.html', 'divination/i18n/ru-ru/templates/divination-description.tpl.html', 'divination/i18n/ru-ru/templates/divination-list.tpl.html', 'divination/i18n/ru-ru/templates/divination-result.tpl.html', 'divination/i18n/uk-ua/templates/divination-description.tpl.html', 'divination/i18n/uk-ua/templates/divination-list.tpl.html', 'divination/i18n/uk-ua/templates/divination-result.tpl.html', 'header/header.tpl.html', 'home/i18n/en-us/templates/home.tpl.html', 'home/i18n/ru-ru/templates/home.tpl.html', 'home/i18n/uk-ua/templates/home.tpl.html', 'mainMenu/mainMenu.tpl.html']);

angular.module("divination/i18n/en-us/templates/divination-description.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/en-us/templates/divination-description.tpl.html",
    "<h3>{{ DivinationDescCtrl.currentDivination.name }}</h3>\n" +
    "\n" +
    "<p>{{ DivinationDescCtrl.currentDivination.description }}</p>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<p class=\"common_paragraph\">\n" +
    "    <form name=\"question\" novalidate>\n" +
    "        Ask your question to Runes:&nbsp;\n" +
    "        <input type=\"text\"\n" +
    "               class=\"question_to_runes\"\n" +
    "               name=\"question_to_runes\"\n" +
    "               data-ng-model=\"DivinationDescCtrl.question\"\n" +
    "               placeholder=\"{{ DivinationDescCtrl.questionPlaceHolder }}\" />\n" +
    "\n" +
    "        <button type=\"submit\"\n" +
    "                data-ng-click=\"DivinationDescCtrl.answerQuestion()\">\n" +
    "            {{ DivinationDescCtrl.getAnswer }}\n" +
    "        </button>\n" +
    "    </form>\n" +
    "</p>\n" +
    "\n" +
    "<hr />");
}]);

angular.module("divination/i18n/en-us/templates/divination-list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/en-us/templates/divination-list.tpl.html",
    "<div>\n" +
    "    <h3>Select the divination below:</h3>\n" +
    "    <ul>\n" +
    "        <li data-ng-repeat=\"divination in DivinationListCtrl.divinations\">\n" +
    "            <a ui-sref=\"appState.divination.description({ divinationId: divination.id })\">{{ divination.name }}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <div data-ui-view=\"divination_description\"></div>\n" +
    "\n" +
    "    <div data-ui-view=\"divination_answer\"></div>\n" +
    "</div>");
}]);

angular.module("divination/i18n/en-us/templates/divination-result.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/en-us/templates/divination-result.tpl.html",
    "<div><h2>Rune's Answer</h2></div>\n" +
    "\n" +
    "<div ng-repeat=\"rune in DivinationResultCtrl.result\">\n" +
    "    <div class=\"one_rune_result_block\">\n" +
    "        <hr />\n" +
    "        <div ng-if=\"rune.time\"><h3>{{ rune.time }}</h3></div>\n" +
    "\n" +
    "        <div id=\"answer_rune_img\">\n" +
    "            <img ng-src=\"{{ rune.runeImage }}\" />\n" +
    "        </div>\n" +
    "\n" +
    "        <div id=\"answer_rune_name\">\n" +
    "            <p>Rune: <strong class=\"answer_name_and_state\">{{ rune.name }}</strong>. Position:&nbsp;\n" +
    "                <strong class=\"answer_name_and_state\">{{ rune.runeState }}</strong></p>\n" +
    "        </div>\n" +
    "\n" +
    "        <div>\n" +
    "            <p class=\"answer_rune_description\">{{ rune.resultDescription }}</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div id=\"ask_another_question\"  class=\"one_rune_result_block\">\n" +
    "    <a ui-sref=\"appState.divination.description({divinationId : DivinationResultCtrl.currentDivination.id})\">\n" +
    "        Ask another question to the Runes\n" +
    "    </a>\n" +
    "</div>");
}]);

angular.module("divination/i18n/ru-ru/templates/divination-description.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/ru-ru/templates/divination-description.tpl.html",
    "<h3>{{ DivinationDescCtrl.currentDivination.name }}</h3>\n" +
    "\n" +
    "<p>{{ DivinationDescCtrl.currentDivination.description }}</p>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<p class=\"common_paragraph\">\n" +
    "    <form name=\"question\" novalidate>\n" +
    "        Задайте свой вопрос Рунам:&nbsp;\n" +
    "        <input type=\"text\"\n" +
    "               class=\"question_to_runes\"\n" +
    "               name=\"question_to_runes\"\n" +
    "               data-ng-model=\"DivinationDescCtrl.question\"\n" +
    "               placeholder=\"{{ DivinationDescCtrl.questionPlaceHolder }}\" />\n" +
    "\n" +
    "        <button type=\"submit\"\n" +
    "                data-ng-click=\"DivinationDescCtrl.answerQuestion()\">\n" +
    "            {{ DivinationDescCtrl.getAnswer }}\n" +
    "        </button>\n" +
    "    </form>\n" +
    "</p>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("divination/i18n/ru-ru/templates/divination-list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/ru-ru/templates/divination-list.tpl.html",
    "<div>\n" +
    "    <h3>Выберите вид гадания из списка:</h3>\n" +
    "    <ul>\n" +
    "        <li data-ng-repeat=\"divination in DivinationListCtrl.divinations\">\n" +
    "            <a href=\"#/divination/description/{{ divination.id }}\">{{ divination.name }}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <div data-ui-view=\"divination_description\"></div>\n" +
    "</div>");
}]);

angular.module("divination/i18n/ru-ru/templates/divination-result.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/ru-ru/templates/divination-result.tpl.html",
    "<div><h2>Ответ Рун</h2></div>\n" +
    "\n" +
    "<div ng-repeat=\"rune in DivinationResultCtrl.result\">\n" +
    "    <div class=\"one_rune_result_block\">\n" +
    "        <hr />\n" +
    "        <div ng-if=\"rune.time\"><h3>{{ rune.time }}</h3></div>\n" +
    "\n" +
    "        <div id=\"answer_rune_img\">\n" +
    "            <img ng-src=\"{{ rune.runeImage }}\" />\n" +
    "        </div>\n" +
    "\n" +
    "        <div id=\"answer_rune_name\">\n" +
    "            <p>Руна: <strong class=\"answer_name_and_state\">{{ rune.name }}</strong>. Положение:&nbsp;\n" +
    "                <strong class=\"answer_name_and_state\">{{ rune.runeState }}</strong></p>\n" +
    "        </div>\n" +
    "\n" +
    "        <div>\n" +
    "            <p class=\"answer_rune_description\">{{ rune.resultDescription }}</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div id=\"ask_another_question\"  class=\"one_rune_result_block\">\n" +
    "    <a ui-sref=\"appState.divination.description({divinationId : DivinationResultCtrl.currentDivination.id})\">\n" +
    "        Задать Рунам еще вопрос\n" +
    "    </a>\n" +
    "</div>");
}]);

angular.module("divination/i18n/uk-ua/templates/divination-description.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/uk-ua/templates/divination-description.tpl.html",
    "<h3>{{ DivinationDescCtrl.currentDivination.name }}</h3>\n" +
    "\n" +
    "<p>{{ DivinationDescCtrl.currentDivination.description }}</p>\n" +
    "\n" +
    "<hr />\n" +
    "\n" +
    "<p class=\"common_paragraph\">\n" +
    "    <form name=\"question\" novalidate>\n" +
    "        Задайте своє питання Рунам:&nbsp;\n" +
    "        <input type=\"text\"\n" +
    "               class=\"question_to_runes\"\n" +
    "               name=\"question_to_runes\"\n" +
    "               data-ng-model=\"DivinationDescCtrl.question\"\n" +
    "               placeholder=\"{{ DivinationDescCtrl.questionPlaceHolder }}\" />\n" +
    "\n" +
    "        <button type=\"submit\"\n" +
    "                data-ng-click=\"DivinationDescCtrl.answerQuestion()\">\n" +
    "            {{ DivinationDescCtrl.getAnswer }}\n" +
    "        </button>\n" +
    "    </form>\n" +
    "</p>\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("divination/i18n/uk-ua/templates/divination-list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/uk-ua/templates/divination-list.tpl.html",
    "<div>\n" +
    "    <h3>Select the divination below:</h3>\n" +
    "    <ul>\n" +
    "        <li data-ng-repeat=\"divination in DivinationListCtrl.divinations\">\n" +
    "            <a href=\"#/divination/description/{{ divination.id }}\">{{ divination.name }}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <div data-ui-view=\"divination_description\"></div>\n" +
    "</div>");
}]);

angular.module("divination/i18n/uk-ua/templates/divination-result.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("divination/i18n/uk-ua/templates/divination-result.tpl.html",
    "<div><h2>Відповідь Рун</h2></div>\n" +
    "\n" +
    "<div ng-repeat=\"rune in DivinationResultCtrl.result\">\n" +
    "    <div class=\"one_rune_result_block\">\n" +
    "        <hr />\n" +
    "        <div ng-if=\"rune.time\"><h3>{{ rune.time }}</h3></div>\n" +
    "\n" +
    "        <div id=\"answer_rune_img\">\n" +
    "            <img ng-src=\"{{ rune.runeImage }}\" />\n" +
    "        </div>\n" +
    "\n" +
    "        <div id=\"answer_rune_name\">\n" +
    "            <p>Руна: <strong class=\"answer_name_and_state\">{{ rune.name }}</strong>. Положення:&nbsp;\n" +
    "                <strong class=\"answer_name_and_state\">{{ rune.runeState }}</strong></p>\n" +
    "        </div>\n" +
    "\n" +
    "        <div>\n" +
    "            <p class=\"answer_rune_description\">{{ rune.resultDescription }}</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div id=\"ask_another_question\"  class=\"one_rune_result_block\">\n" +
    "    <a ui-sref=\"appState.divination.description({divinationId : DivinationResultCtrl.currentDivination.id})\">\n" +
    "        Задати Рунам інше питання\n" +
    "    </a>\n" +
    "</div>");
}]);

angular.module("header/header.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("header/header.tpl.html",
    "<div id=\"header_wrapper\">\n" +
    "    <div id=\"header_banner\">Header banner</div>\n" +
    "\n" +
    "    <language-switcher></language-switcher>\n" +
    "\n" +
    "    <div id=\"header_menu\"\n" +
    "         data-ng-controller=\"MainMenuCtrl\"\n" +
    "         data-ng-include=\"'mainMenu/mainMenu.tpl.html'\">\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("home/i18n/en-us/templates/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/i18n/en-us/templates/home.tpl.html",
    "<h1>Welcome to the World of Runes!</h1>\n" +
    "\n" +
    "<div>\n" +
    "    <p class=\"common_paragraph\">Welcome to the site related to Scandinavian runes. It's quite a common situation in life when it is\n" +
    "        unclear what should be done and what is the correct way for movement. And you want to have an advice.\n" +
    "        At these moments people search for answers in divination.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">Хотя, на самом деле, все ответы уже есть внутри нас, внутри нашего подсознания и все, что необходимо -\n" +
    "        это извлечь их оттуда.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">В древнескандинавские руны можно верить - можно не верить, но дать они всегда могут послужить тем\n" +
    "        толчком для извлечения ответа на насущные вопросы из нашей гловы.\n" +
    "    </p>\n" +
    "\n" +
    "    <p class=\"common_paragraph\">\n" +
    "        <a href=\"#/divination\">\n" +
    "            <strong>Перейти к гаданиям</strong>\n" +
    "        </a>\n" +
    "    </p>\n" +
    "</div>");
}]);

angular.module("home/i18n/ru-ru/templates/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/i18n/ru-ru/templates/home.tpl.html",
    "<h1>Добро пожаловать в мир Рун</h1>\n" +
    "\n" +
    "<div>\n" +
    "    <p class=\"common_paragraph\">Добро пожаловать на сайт мистерий скандинавских рун. Часто в жизни возникают ситуации, когда непонятно,\n" +
    "        что делать и куда двигаться. И хочеться получить совет. И тогда люди прибегают к разным видам гаданий\n" +
    "        и предсказаний.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">Хотя, на самом деле, все ответы уже есть внутри нас, внутри нашего подсознания и все, что необходимо -\n" +
    "        это извлечь их оттуда.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">В древнескандинавские руны можно верить - можно не верить, но дать они всегда могут послужить тем\n" +
    "        толчком для извлечения ответа на насущные вопросы из нашей гловы.\n" +
    "    </p>\n" +
    "\n" +
    "    <p class=\"common_paragraph\">\n" +
    "        <a href=\"#/divination\">\n" +
    "            <strong>Перейти к гаданиям</strong>\n" +
    "        </a>\n" +
    "    </p>\n" +
    "</div>");
}]);

angular.module("home/i18n/uk-ua/templates/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/i18n/uk-ua/templates/home.tpl.html",
    "<h1>Ласкаво просимо у світ Рун!</h1>\n" +
    "\n" +
    "<div>\n" +
    "    <p class=\"common_paragraph\">Ласкаво просимо до світу містерій скандинавських Рун. Досить часто в нашому житті виникають ситуації,\n" +
    "        коли незрозуміло, що робити та куди рухатись далі. І потрібна порада. Саме тоді люди вдаються до різних ворожінь і пророкувань.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">Насправді, всі відповіді вже є внутри нас самих. Все, що потрібно - це побачити їх.\n" +
    "    </p>\n" +
    "    <p class=\"common_paragraph\">У давньоскандинавські руни можно вірити або ні. але вони можуть бути поштовхом,\n" +
    "        щоб знайти відповідь на насущне питання.\n" +
    "    </p>\n" +
    "\n" +
    "    <p class=\"common_paragraph\">\n" +
    "        <a href=\"#/divination\">\n" +
    "            <strong>Перейти до ворожіння</strong>\n" +
    "        </a>\n" +
    "    </p>\n" +
    "</div>");
}]);

angular.module("mainMenu/mainMenu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("mainMenu/mainMenu.tpl.html",
    "<ul id=\"main_menu_container\">\n" +
    "    <li class=\"main_menu_item\">\n" +
    "        <a href=\"#/home\" class=\"main_menu_item\">{{ MainMenuCtrl.homeMenuItem }}</a>\n" +
    "    </li>\n" +
    "\n" +
    "    <li class=\"main_menu_item\">\n" +
    "        <a href=\"#/divination\" class=\"main_menu_item\">{{ MainMenuCtrl.divinationsMenuItem }}</a>\n" +
    "    </li>\n" +
    "\n" +
    "    <!--<li class=\"main_menu_item\">-->\n" +
    "        <!--<a href=\"#\" class=\"main_menu_item\">{{ MainMenuCtrl.runesMenuItem }}</a>-->\n" +
    "    <!--</li>-->\n" +
    "</ul>");
}]);
