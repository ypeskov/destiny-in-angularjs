angular.module('app', [
    'ngLocale',
    'templates-app',
    'templates-common',
    'ui.router',
    'app.appLocale',

    'app.home',
    'app.header',
    'app.mainMenu',
    'app.locale',
    'app.divination'
])

.config(['$stateProvider', '$urlRouterProvider',
function ($stateProvider, $urlRouterProvider) {
    "use strict";
    var _self = this;

    /**
     * returns translations for the current locale or promise for them
     *
     * @param appTranslation
     * @returns {*}
     */
    function promiseOrTranslatedStrings(appTranslation)
    {
        if ( appTranslation.translatedStrings.is_translated ) {
            return appTranslation.translatedStrings;
        } else {
            return appTranslation.loadStrings().then(
            function(response) {
                return response.data;
            },
            function(response) { alert('Error'); }
            );
        }
    }

    /**
     * returns promise for divinations list or divinations if they are already available.
     *
     * @param divinationFactory
     * @returns {*}
     */
    function promiseOrDivinations(divinationFactory)
    {
        if ( divinationFactory.divinations.length === 0 ) {
            return divinationFactory.getAllDivinations().then(
            function(response) {
                divinationFactory.divinations = response.data;

                return response.data;
            },
            function(response) {}
            );
        } else {
            return divinationFactory.divinations;
        }
    }

    $stateProvider.state('appState', {
        abstract:   true,
        views:  {
            'main': {
                template: '<div data-ui-view="main"></div> '
            }
        },
        resolve: {
            'translatedStrings': function(appTranslation) {
                return promiseOrTranslatedStrings(appTranslation);
            },
            'divinations': function(divinationFactory, appTranslation) {
                return promiseOrDivinations(divinationFactory);
            }
        }
    });

    $urlRouterProvider.otherwise('/home');
}])

.run(['$state', 'appTranslation', 'divinationFactory', function($state, appTranslation, divinationFactory) {
    "use strict";



}])

.controller('AppCtrl', ['$scope', '$location', 'appTranslation', '$state',
function ($scope, $location, appTranslation, $state) {
    "use strict";
    var _self = this;

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if ( angular.isDefined(toState.data.pageTitle) ) {
            _self.pageTitle = toState.data.pageTitle;
        }
    });

    return $scope.AppCtrl = _self;
}])


;

