angular.module('app.appLocale', [
    'ngLocale'
])

.provider('appLocale', ['APP_LOCALE_CONFIG', function(APP_LOCALE_CONFIG) {
    "use strict";
    var _self = this;

    var injector = angular.injector(['ngLocale']);
    var $locale = injector.get('$locale');

    return {
        getLocaleId: function() {
            var _self = this;

            return $locale.id;
        },

        $get: function() { },

        getTemplatePath: function(localeId, moduleDirName, templateName) {
            var _self = this;

            return moduleDirName + '/' + APP_LOCALE_CONFIG.I18N_DIR + '/' + localeId + '/templates/' + templateName + '.tpl.html';
        }
    };
}])

.factory('appTranslation', ['$interpolate', '$locale', '$http',
function($interpolate, $locale, $http) {
    var _self = this;

    return {
        /**
         * Keep our translations here
         */
        translatedStrings: {},

        generalError: false,

        getStrings: function() {
            return this.translatedStrings;
        },

        /**
         * Loads an object of string templates in the definite locale
         *
         * @returns {Array}
         */
        loadStrings: function() {
            var _self = this;

            path = '/locale/all?locale=' + $locale.id;

            var promise = $http.get(path)
            .success(function(data, status, headers, config) {
                if (data.is_translated === true ) {
                    _self.translatedStrings = data;
                } else {
                    _self.generalError = true;
                }
            })
            .error(function(data, status, headers, config) {
                if ( status !== 200 ) {
                    _self.generalError = true;

                    //TODO: think how to process error and remove alert
                    alert('Error getting translated strings');
                }
            });

            return promise;
        },

        /**
         * Return the string for the current locale.
         * If the key doesn't exists returns itself.
         *
         * @param key
         * @returns String
         */
        getString: function(key) {
            var _self = this;

            for(var k in _self.translatedStrings) {
                if ( key === k ) {
                    return _self.translatedStrings[k];
                }
            }

            return key;
        },

        ucfirst: function(str) {
            var firstLetter = str.charAt(0).toUpperCase();

            return firstLetter + str.substr(1, str.length-1);
        }
    };
}])

.constant('APP_LOCALE_CONFIG', {
    I18N_DIR:           'i18n',
    DEFAULT_LOCALE:     'en-us'
})

;