angular.module('app.home', [
    'ui.router',
    'app.appLocale'
])

.config(['$stateProvider', 'appLocaleProvider', 'homeConst',
function($stateProvider, appLocaleProvider, homeConst) {
    "use strict";
    var _self = this;

    var moduleName = homeConst.moduleName;
    var localeId = appLocaleProvider.getLocaleId();
    var tplPath = appLocaleProvider.getTemplatePath(localeId, moduleName, moduleName);

    $stateProvider.state(homeConst.moduleState, {
        url: homeConst.moduleUrl,
        views: {
            'main': {
                controller:     homeConst.moduleCtrl,
                templateUrl:    tplPath
            }
        },
        data: {
            pageTitle: homeConst.modulePageTitle
        }
    });

}])

.controller('HomeCtrl', ['$scope',
function ($scope) {
    "use strict";
    var _self = this;

    return $scope.HomeCtrl = _self;
}])

.constant('homeConst', {
    'moduleName':       'home',
    'moduleState':      'home',
    'moduleUrl':        '/home',
    'moduleCtrl':       'HomeCtrl',
    'modulePageTitle':  'Home'
})

;
