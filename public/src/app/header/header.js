angular.module('app.header', [
    'ui.router'
])

.controller('HeaderCtrl', ['$scope', function($scope) {
    "use strict";
    var _self = this;

    return $scope.HeaderCtrl = _self;
}])

;