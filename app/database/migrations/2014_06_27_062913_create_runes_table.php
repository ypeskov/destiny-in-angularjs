<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $tableName = (new Rune())->getTable();

		Schema::create($tableName, function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name_key', 20);
            $table->boolean('has_indirect');
            $table->text('direct_description_key');
            $table->text('indirect_description_key');
            $table->string('direct_img', 100);
            $table->string('indirect_img', 100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $tableName = (new Rune())->getTable();

		Schema::drop($tableName);
	}

}
