<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $tableName = (new Divination())->getTable();

		Schema::create($tableName, function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name_key', 50);
            $table->string('description_key', 50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $tableName = (new Divination())->getTable();

		Schema::drop($tableName);
	}

}
