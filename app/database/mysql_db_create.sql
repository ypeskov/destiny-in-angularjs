create database dest_angular default character set = "utf8" collate = "utf8_unicode_ci";

create user 'dest'@'localhost' identified by 'dest';

grant all on dest_angular.* to 'dest'@'localhost' with grant option;