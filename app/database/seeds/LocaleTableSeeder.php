<?php

class LocaleTableSeeder extends Seeder
{
    public function run()
    {
        $localesTableName = (new Locale())->getTable();

        DB::table($localesTableName)->delete();

        Locale::create([
            'value'         => 'en-us',
            'name'          => 'English (US)',
            'native_name'   => 'English (US)',
        ]);

        Locale::create([
            'value'         => 'uk-ua',
            'name'          => 'Ukrainian (UA)',
            'native_name'   => 'Українська (УА)',
        ]);

        Locale::create([
            'value'         => 'ru-ru',
            'name'          => 'Russian (RU)',
            'native_name'   => 'Русский (РУ)',
        ]);
    }
}