<?php

class RuneTableSeeder extends Seeder
{
    public function run()
    {
        $tableName = (new Rune())->getTable();

        DB::table($tableName)->delete();

        Rune::create([
            'name_key'                  => 'fehu_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'fehu_direct_description',
            'indirect_description_key'  => 'fehu_indirect_description',
            'direct_img'                => 'fehu.gif',
            'indirect_img'              => 'fehureverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'uruz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'uruz_direct_description',
            'indirect_description_key'  => 'uruz_indirect_description',
            'direct_img'                => 'uruz.gif',
            'indirect_img'              => 'uruzreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'turisaz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'turisaz_direct_description',
            'indirect_description_key'  => 'turisaz_indirect_description',
            'direct_img'                => 'turisaz.gif',
            'indirect_img'              => 'turisazreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'ansuz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'ansuz_direct_description',
            'indirect_description_key'  => 'ansuz_indirect_description',
            'direct_img'                => 'ansuz.gif',
            'indirect_img'              => 'ansuzreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'raidho_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'raidho_direct_description',
            'indirect_description_key'  => 'raidho_indirect_description',
            'direct_img'                => 'raidho.gif',
            'indirect_img'              => 'raidhoreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'kenaz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'kenaz_direct_description',
            'indirect_description_key'  => 'kenaz_indirect_description',
            'direct_img'                => 'kenaz.gif',
            'indirect_img'              => 'kenazreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'gebo_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'gebo_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'gebo.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'wunjo_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'wunjo_direct_description',
            'indirect_description_key'  => 'wunjo_indirect_description',
            'direct_img'                => 'wunjo.gif',
            'indirect_img'              => 'wunjoreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'hagalaz_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'hagalaz_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'hagalaz.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'nautiz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'nautiz_direct_description',
            'indirect_description_key'  => 'nautiz_indirect_description',
            'direct_img'                => 'nautiz.gif',
            'indirect_img'              => 'nautizreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'isa_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'isa_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'isa.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'jera_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'jera_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'jera.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'eihwaz_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'eihwaz_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'eihwaz.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'perth_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'perth_direct_description',
            'indirect_description_key'  => 'perth_indirect_description',
            'direct_img'                => 'perth.gif',
            'indirect_img'              => 'perthreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'algiz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'algiz_direct_description',
            'indirect_description_key'  => 'algiz_indirect_description',
            'direct_img'                => 'algiz.gif',
            'indirect_img'              => 'algizreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'sol_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'sol_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'soul.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'teihwaz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'teihwaz_direct_description',
            'indirect_description_key'  => 'teihwaz_indirect_description',
            'direct_img'                => 'teihwaz.gif',
            'indirect_img'              => 'teihwazreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'berkana_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'berkana_direct_description',
            'indirect_description_key'  => 'berkana_indirect_description',
            'direct_img'                => 'berkana.gif',
            'indirect_img'              => 'berkanareverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'ehwaz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'ehwaz_direct_description',
            'indirect_description_key'  => 'ehwaz_indirect_description',
            'direct_img'                => 'ewaz.gif',
            'indirect_img'              => 'ewazreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'mannaz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'mannaz_direct_description',
            'indirect_description_key'  => 'mannaz_indirect_description',
            'direct_img'                => 'mannaz.gif',
            'indirect_img'              => 'mannazreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'laguz_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'laguz_direct_description',
            'indirect_description_key'  => 'laguz_indirect_description',
            'direct_img'                => 'laguz.gif',
            'indirect_img'              => 'laguzreverse.gif',
        ]);

        Rune::create([
            'name_key'                  => 'inguz_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'inguz_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'inguz.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'dagaz_name',
            'has_indirect'              => 0,
            'direct_description_key'    => 'dagaz_direct_description',
            'indirect_description_key'  => '',
            'direct_img'                => 'dagaz.gif',
            'indirect_img'              => '',
        ]);

        Rune::create([
            'name_key'                  => 'othila_name',
            'has_indirect'              => 1,
            'direct_description_key'    => 'othila_direct_description',
            'indirect_description_key'  => 'othila_indirect_description',
            'direct_img'                => 'othal.gif',
            'indirect_img'              => 'othalreverse.gif',
        ]);
    }
}