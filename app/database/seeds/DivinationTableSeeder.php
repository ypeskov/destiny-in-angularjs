<?php
/**
 * remember-calories.com (c) 2010-2014
 * Author: ${MyName}
 * Email: ${MyEmail}
 * Date: 5/30/14
 * Time: 9:20 AM
 */

class DivinationTableSeeder extends Seeder
{
    public function run()
    {
        $tableName = (new Divination())->getTable();

        DB::table($tableName)->delete();

        Divination::create([
            'name_key'          => 'one_rune_divination',
            'description_key'   => 'one_rune_description',
        ]);

        Divination::create([
            'name_key'          => 'three_runes_divination',
            'description_key'   => 'three_runes_description',
        ]);
    }
} 