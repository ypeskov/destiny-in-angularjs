<?php
class Translation extends Eloquent
{
    protected $table = 'translations';

    public function locale()
    {
        return $this->belongsTo('Locale');
    }
}