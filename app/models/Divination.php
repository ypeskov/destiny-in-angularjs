<?php
/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 5/25/14
 * Time: 5:19 PM
 */

class Divination extends Eloquent
{
    protected $table = 'divinations';

    public function translations()
    {
        return $this->hasMany('Translation');
    }
}