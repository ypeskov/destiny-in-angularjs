<?php

class Locale extends Eloquent
{
    protected $table = 'locales';

    public function translations()
    {
        return $this->hasMany('Translation', 'locale_id', 'value');
    }
}