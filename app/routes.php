<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', [
    'as'    => 'home',
    'uses'  => 'HomeController@indexAction',
]);

Route::get('/locale', [
    'as'    => 'getLocales',
    'uses'  => 'LocaleController@getLocalesAction',
]);

Route::get('/locale/all', [
    'as'    => 'getTranslatedStrings',
    'uses'  => 'LocaleController@getTranslatedStringsAction',
]);

Route::get('/divination/all', [
    'as'    => 'getAllDivinations',
    'uses'  => 'DivinationController@getAllDivinationsAction',
]);

//TODO: change after development any -> post
Route::any('/result/get', [
    'as'    => 'getResult',
    'uses'  => 'DivinationController@getResultAction',
]);