<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}


    public function getResponse($returnCode, $isSuccess, $result, $returnMsg='')
    {
        return [
            'SUCCESS'   => $isSuccess,
            'CODE'      => $returnCode,
            'MSG'       => $returnMsg,
            'RESULT'    => $result,
        ];
    }
}
