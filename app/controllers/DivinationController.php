<?php
/**
 * remember-calories.com (c) 2010-2014
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 5/25/14
 * Time: 5:12 PM
 */

use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Divination\DivinationFactory;


class DivinationController extends BaseController
{
    protected $divinationFactory;

    public function __construct(DivinationFactory $divinationFactory)
    {
        $this->divinationFactory = $divinationFactory;
    }

    /**
     * Returns the list of all divinations.
     * Note: name and desription are only keys. Translation for them are in another table.
     *
     * @return JSON
     */
    public function getAllDivinationsAction()
    {
        $divinations = Divination::all();

        return Response::json($divinations);
    }

    /**
     * Returns a result of divination
     *
     * @return JSON
     * @throws Exception
     */
    public function getResultAction()
    {
        $divinationId   = Input::get('divinationId', null);
        $question       = Input::get('question', null);

        if ( $divinationId === null ) {
            throw new Exception('DIVINATION_ID_IS_REQUIRED');
        }

        if ( $question === null ) {
            throw new Exception('QUESTION_IS_REQUIRED');
        }

        //check whether ID is correct
        $this->divinationFactory->getDivinationById($divinationId);

        $divination = $this->divinationFactory->factory($divinationId);

        $result = $divination->getResult();

        return Response::json($this->getResponse(0, true, $result, ''));
    }
}