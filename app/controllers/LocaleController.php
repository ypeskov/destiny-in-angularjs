<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class LocaleController extends BaseController
{
    public function getLocalesAction()
    {
        $locales = Locale::all()->toArray();

        return Response::json($locales);
    }

    /**
     * Reads localized strings from DB according to the locale and
     * returns a JSON string with them.
     *
     * @return JSON
     */
    public function getTranslatedStringsAction()
    {
        $localeName = Input::get('locale', 'en-us');

        $responseArr = [];
        try {
            $locale = Locale::where('value', '=', $localeName)->firstOrFail();
            $strings = Translation::where('locale_id', '=', $locale->id)->get();

            foreach($strings as $key => $value) {
                $responseArr[$value->key] = $value->string;
            }

            $responseArr['is_translated'] = true;
        } catch(ModelNotFoundException $e) {
            $responseArr['is_translated'] = false;
        }

        return Response::json($responseArr);
    }
}