<?php

class DivinationControllerTest extends TestCase {
    public function testGetResultActionQuestionRequired() {
        try {
            $response = $this->action('POST', 'DivinationController@getResultAction', ['divinationId' => 1]);
        } catch(Exception $e) {
            $this->assertEquals($e->getMessage(),'QUESTION_IS_REQUIRED');
        }
    }

    public function testGetResultActionDivinationIdRequired() {
        try {
            $response = $this->action('POST', 'DivinationController@getResultAction', ['question' => '']);
        } catch (Exception$e) {
            $this->assertEquals($e->getMessage(), 'DIVINATION_ID_IS_REQUIRED');
        }
    }

    public function testGetResultActionOneRuneDivination() {
        $response = $this->action('POST', 'DivinationController@getResultAction', ['question' => '', 'divinationId' => 1]);

        $this->assertResponseOk();
        $this->assertEquals($response->headers->get('Content-Type'), 'application/json');

        //test that there are 2 divinations in response
        $jsonDataArr = $response->getData();
        $this->assertTrue($jsonDataArr->SUCCESS);
        $this->assertEquals(gettype($jsonDataArr->RESULT), 'object');
        $this->assertEquals(get_class($jsonDataArr->RESULT), 'stdClass');
    }

    public function testGetAllDivinationsAction() {
        $response = $this->action('GET', 'DivinationController@getAllDivinationsAction');

        $this->assertResponseOk();
        $this->assertEquals($response->headers->get('Content-Type'), 'application/json');

        //test that there are 2 divinations in response
        $jsonDataArr = $response->getData();
        $this->assertEquals(sizeof($jsonDataArr), 2);
    }
}