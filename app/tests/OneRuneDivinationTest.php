<?php

use Divination\OneRuneDivination;

class OneRuneDivinationTest extends TestCase {
    public function testThatAllRunesInRandomScope() {
        //prepare the list of rune Ids
        $runesIds = [];
        for($i=1; $i <= 24; $i++) {
            $runesIds[] = $i;
        }

        $divination = new OneRuneDivination();

        $tries = 0;
        while( sizeof($runesIds) > 0 ) {
            $result = $divination->getResult();
            $runeId = (Integer) $result['id'];
            $key = array_search($runeId, $runesIds);
            unset($runesIds[$key]);

            $tries++;
        }

        echo "\n============================================\n";
        echo "One Rune Divination random tries.\n";
        echo "The total quantity of tries = " . $tries;
        echo "\n============================================\n";
    }
}