<?php

use Divination\DivinationFactory;

class DivinationFactoryTest extends TestCase {
	public function testIsOneRuneDivinationAvailable() {
        $oneRuneDivinationId = 1;

        $divFactory = new DivinationFactory();
        $oneRuneDivination = $divFactory->factory($oneRuneDivinationId);
        $this->assertEquals(get_class($oneRuneDivination), 'Divination\OneRuneDivination');
	}
}
