<?php

namespace Divination;

class OneRuneDivination extends BaseDivination {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Returns the result for One Rune Divination: one rune Array.
     *
     * @return Array
     */
    public function getResult() {
        $numberOfRunes = $this->runes->count();

        $rune = $this->getOneRune($numberOfRunes);

        return [$rune];
    }
}