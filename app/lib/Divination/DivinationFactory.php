<?php

namespace Divination;

use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class DivinationFactory
{
    protected $availableDivinations;
    protected $divinationClassesMap;

    const ONE_RUNE_DIVINATION       = 1;
    const THREE_RUNES_DIVINATION    = 2;

    public function __construct()
    {
        $this->fillAvailableDivinations();
        $this->setDivinationClassesMap();
    }

    /**
     * Configure the map of classes for each type of Divination.
     * It is used later for getting an instance for divination.
     */
    protected function setDivinationClassesMap()
    {
        $this->divinationClassesMap = [
            self::ONE_RUNE_DIVINATION       => 'OneRuneDivination',
            self::THREE_RUNES_DIVINATION    => 'ThreeRunesDivination',
        ];
    }

    /**
     * Gets all divinations from DB and sets them to a property $availableDivinations
     */
    protected function fillAvailableDivinations()
    {
        $this->availableDivinations = \Divination::all();
    }

    /**
     * Check if ID is valid for existing and available Divination
     *
     * @param $divinationId
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getDivinationById($divinationId)
    {
        foreach($this->availableDivinations as $key => $divination) {
            if ( $divination->id === $divinationId) {
                return $divination;
            }
        }

        throw new NotFoundHttpException('DIVINATION_ID_NOT_FOUND');
    }

    /**
     * Create and return an instance of the appropriate Divination class.
     *
     * @param $divinationId
     * @return mixed
     */
    public function factory($divinationId)
    {
        $className = __NAMESPACE__ . '\\' . $this->divinationClassesMap[(int) $divinationId];

        return new $className();
    }
}