<?php
namespace Divination;

class ThreeRunesDivination extends BaseDivination {
    const NUMBER_OF_RUNES = 3;

    protected $period = ['past', 'present', 'future'];

    public function getResult() {
        $result = $this->getNumberOfRunes(self::NUMBER_OF_RUNES);

        // Set accordance of rune and time
        for($i=0; $i < self::NUMBER_OF_RUNES; $i++) {
            $result[$i]['timePeriod'] = $this->period[$i];
        }

        return $result;
    }
}