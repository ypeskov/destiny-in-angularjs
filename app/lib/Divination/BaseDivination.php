<?php

namespace Divination;

class BaseDivination {
    protected $runes;

    public function __construct() {
        $this->runes = \Rune::all();
    }

    /**
     * Returns random quantity of non-repeating runes.
     *
     * @param $quantityOfRunes
     * @return Rune object model.
     */
    protected function getNumberOfRunes($quantityOfRunes) {
        $runesLeft = $this->runes->toArray();

        $runesToReturn = [];
        for($i=0; $i < $quantityOfRunes; $i++) {
            $runeId = mt_rand(1, sizeof($runesLeft)) - 1;

            $rune = $runesLeft[$runeId];
            $rune['isDirect'] = $this->getPosition($rune);
            $runesToReturn[] = $rune;

            unset($runesLeft[$runeId]);
            $runesLeft = array_values($runesLeft);
        }

        return $runesToReturn;
    }

    /**
     * Selects randomly one rune from array.
     *
     * @param $numberOfRunes
     * @return mixed
     */
    protected function getOneRune($numberOfRunes) {
        //select a random rune from existing
        $runeSelectedId = mt_rand(1, $numberOfRunes);

        $rune = $this->runes[$runeSelectedId - 1]->toArray();
        $rune['isDirect'] = $this->getPosition($rune);

        return $rune;
    }

    /**
     * Returns random direct\reversed position for the rune.
     *
     * @param $rune
     * @return bool
     */
    protected function getPosition($rune) {
        $isDirect = true;

        if ( (int) $rune['has_indirect'] ) {
            $isDirect = ! (mt_rand(0,1));
        }

        return $isDirect;
    }
}