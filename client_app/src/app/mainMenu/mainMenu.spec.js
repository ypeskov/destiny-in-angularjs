describe('verify menu building', function() {
    var $rootScope, $scope, createCtrl, $location, mockAppTranslation, deferredQuery, response;

    beforeEach(module('app.mainMenu'));
    beforeEach(module('app.appLocale'));

    response = {};
    response.data = {
        homeMenu:   'mockHomeMenu',
        divineMenu: 'mockListDivinations',
        runesMenu:  'mockRunes'
    };

    beforeEach(inject(function($injector) {
        $location   = $injector.get('$location');
        var $q      = $injector.get('$q');
        $rootScope  = $injector.get('$rootScope');
        $scope      = $rootScope.$new();

        var $controller = $injector.get('$controller');

        mockAppTranslation = {
            loadStrings: function() {
                deferredQuery   = $q.defer();
                return deferredQuery.promise;
            }
        };

        spyOn(mockAppTranslation, 'loadStrings').andCallThrough();

        createCtrl = function() {
            var ct = $controller('MainMenuCtrl', {
                '$scope':           $scope,
                'appTranslation':   mockAppTranslation
            });

            return ct;
        };
    }));

    it('Test that controller exists', inject(function() {
        var ctrl = createCtrl();

        expect(ctrl).toBeTruthy();

        deferredQuery.resolve(response);
        $scope.$digest();

        expect(mockAppTranslation.loadStrings).toHaveBeenCalled();
        expect(ctrl.homeMenuItem).toEqual('mockHomeMenu');
        expect(ctrl.divinationsMenuItem).toEqual('mockListDivinations');
        expect(ctrl.runesMenuItem).toEqual('mockRunes');
    }));

    it('Test behavior of controller in case of error loading string', inject(function() {
        var ctrl = createCtrl();

        expect(ctrl).toBeTruthy();

        deferredQuery.reject();
        $scope.$digest();

        expect(ctrl.generalError).toBeTruthy();
    }));
});