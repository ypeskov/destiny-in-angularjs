angular.module('app.mainMenu', [
    'ui.router'
])

.controller('MainMenuCtrl', ['$scope', 'appTranslation',
function($scope, appTranslation) {
    var _self = this;

    appTranslation.loadStrings().then(
    function(response) {
        _self.homeMenuItem = response.data.homeMenu;
        _self.divinationsMenuItem = response.data.divineMenu;
        _self.runesMenuItem = response.data.runesMenu;
    },
    function(response) {
        _self.generalError = true;

        //TODO: think how to process error and remove alert
        alert('Error loading menu translations');
    });

    return $scope.MainMenuCtrl = _self;
}])

;