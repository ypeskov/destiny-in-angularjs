angular.module('app.locale', [
    'ui.router'
])

.directive('languageSwitcher', ['LocaleFactory', 'LocaleConst',
function(localeFactory, LocaleConst) {
    return {
        restrict: 'E',

        replace:    true,

        scope: {},

        template:   '<div class="language_switcher">' +
                    '<select data-ng-change="localeChange()"' +
                    'data-ng-model="currentLocale"' +
                    'data-ng-options="locale.native_name for locale in languages">' +
                    '</select>' +
                    '</div>',

        link: function(scope, element, attrs) {
            localeFactory.updateAvailableLocales().then(
            function(response) {
                scope.currentLocale = localeFactory.currentLocale;
                scope.languages = response.data;
            },
            function(response) {}
            );


            scope.localeChange = function() {
                window.location = LocaleConst.localeGetParam +
                    scope.currentLocale.value;
            };
        }
    };
}])

.factory('LocaleFactory', ['$locale', '$http', 'LocaleConst', 'appTranslation',
function($locale, $http, LocaleConst, appTranslation) {
    "use strict";
    var _self = this;

    return {
        locales:        [],

        currentLocale:  null,

        getCurrentLocale: function() {
            var _self = this;

            return _self.currentLocale;
        },

        updateAvailableLocales: function() {
            var _self = this;

            var promise;
            promise = $http.get(LocaleConst.localeUrl)
            .success(function (data, status, headers, config) {
                _self.locales = data;
                _self.currentLocale = _self.setCurrentLocale();
            })
            .error(function (data, status, headers, config) {
                alert('Error in getting locales. LocaleFactory');
            });

            return promise;
        },

        setCurrentLocale: function() {
            var _self = this;
            var currentLocale = null;

            for(var i = 0; i < _self.locales.length; i++) {
                if ( _self.locales[i].value === $locale.id ) {
                    currentLocale = _self.locales[i];

                    break;
                }
            }

            return currentLocale;
        }
    };
}])

.constant('LocaleConst', {
    'localeUrl':        '/locale',
    'localeGetParam':   '/?locale='
})

;