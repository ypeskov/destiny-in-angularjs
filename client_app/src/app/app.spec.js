describe('AppCtrl', function () {
//---------------------- Variant 1 of mocking controller ------------------------
    describe('Test main application module', function () {
        var AppCtrl, $location, $scope;

        beforeEach(module('app'));

        beforeEach(inject(function ($controller, _$location_, $rootScope) {
            $location   = _$location_;
            $scope      = $rootScope.$new();
            AppCtrl     = $controller('AppCtrl', {
                $location: $location,
                $scope: $scope
            });
        }));


        it('should pass a dummy test', inject(function () {
            expect(AppCtrl).toBeTruthy();
        }));
    });

//----------------------- Variant 2 of mocking controller -----------------------------------
    describe('Test main application module the other way', function () {
        var $location, $rootScope, $scope, createCtrl;

        beforeEach(module('app'));

        beforeEach(inject(function($injector) {
            $location   = $injector.get('$location');
            $rootScope  = $injector.get('$rootScope');
            $scope      = $rootScope.$new();

            var $controller = $injector.get('$controller');

            createCtrl  = function() {
                return $controller('AppCtrl', {
                    '$scope': $scope
                });
            };
        }));

        it('should pass a dummy test', inject(function () {
            var AppCtrl = createCtrl();

            expect(AppCtrl).toBeTruthy();
        }));
    });
});