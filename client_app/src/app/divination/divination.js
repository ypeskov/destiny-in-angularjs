angular.module('app.divination', [
    'ui.router',
    'app.appLocale'
])

//*******************************************************************
.config(['$stateProvider', 'appLocaleProvider', 'DivinationConst',
function($stateProvider, appLocaleProvider, DivinationConst) {
    'use strict';
    var _self = this;

    var moduleName = DivinationConst.moduleName;
    var localeId = appLocaleProvider.getLocaleId();

    var listTplPath = appLocaleProvider.getTemplatePath(localeId, moduleName, DivinationConst.listTplName);

    $stateProvider.state(DivinationConst.moduleState, {
        url:    DivinationConst.moduleUrl,
        views:  {
            'main': {
                controller: DivinationConst.listController,
                templateUrl: listTplPath
            }
        },
        data: {
            pageTitle:  DivinationConst.listPageTitle
        },
        resolve: {
            'translatedStrings': function(translatedStrings) {
                return translatedStrings;
            },
            'divinations': function(divinations) {
                return divinations;
            }
        }
    });

    var descTplPath = appLocaleProvider.getTemplatePath(localeId, moduleName, DivinationConst.divinationDescTplName);

    $stateProvider.state(DivinationConst.divinationDescState, {
        url: DivinationConst.divinationDescUrl,
        views: {
            'divination_description': {
                controller:     DivinationConst.divinationDescController,
                templateUrl:    descTplPath
            }
        },
        data: {
            pageTitle: DivinationConst.divinationDescTitle
        },
        resolve: {
            'translatedStrings': function(translatedStrings) {
                return translatedStrings;
            },
            'divinations': function(divinations) {
                return divinations;
            }
        }
    });

    var ResultTplPath = appLocaleProvider.getTemplatePath(localeId, moduleName, DivinationConst.divinationTypes[0].templateUrl);

    $stateProvider.state(DivinationConst.divinationTypes[0].state, {
        url: DivinationConst.divinationTypes[0].url,
        views: {
            'main': {
                controller:     DivinationConst.divinationTypes[0].controller,
                templateUrl:    ResultTplPath
            }
        },
        data: {
            pageTitle: DivinationConst.divinationTypes[0].pageTitle
        },
        resolve: {
            'translatedStrings': function(translatedStrings) {
                return translatedStrings;
            },
            'divinations': function(divinations) {
                return divinations;
            }
        }
    });

}])

//*******************************************************************
.controller('DivinationListCtrl', ['$scope', 'divinationFactory', 'translatedStrings',
function($scope, divFactory) {
    "use strict";
    var _self = this;

    _self.divinations = divFactory.applyTranslationsToDivinations().getDivinations();

    return $scope.DivinationListCtrl = _self;
}])

//*******************************************************************
.controller('DivinationDescCtrl', ['$scope', '$stateParams', 'divinationFactory', 'appTranslation', '$state',
                                    'DivinationConst',
function($scope, $stateParams, divFactory, appTranslation, $state, DivinationConst) {
    "use strict";
    var _self = this;

    divFactory.result = null;//clear previous result

    _self.currentDivinationId = $stateParams.divinationId;
    _self.currentDivination = divFactory.getOneDivination(_self.currentDivinationId);
    divFactory.setCurrentDivination(_self.currentDivination);

    _self.questionPlaceHolder = appTranslation.getString('type_your_question', appTranslation.translatedStrings);
    _self.question = '';
    _self.getAnswer = appTranslation.getString('get_answer', appTranslation.translatedStrings);

    /**
     * Process the question and give an answer
     */
    _self.answerQuestion = function() {
        divFactory.setQuestion(_self.question);

        divFactory.getResult().then(
        function(response) {
            divFactory.result = response.data.RESULT;
            divFactory.translateResult();

            $state.go(divFactory.getCurrentDivinationState());
        },
        function() {
            alert('Some error in runes answer. Please try again');
            $state.go(DivinationConst.moduleState);
        }
        );
    };

    return $scope.DivinationDescCtrl = _self;
}])

//*******************************************************************
.controller('DivinationResultCtrl', ['$scope', 'divinationFactory', '$state', 'DivinationConst', 'OneRuneDivinationFactory',
function($scope, divFactory, $state, divConst, oneRuneFactory) {
    var _self = this;

    var result = divFactory.result;
    var imgPath = divConst.imagesPath;

    //if result is absent (e.g. page reload) then go to the list of divinations
    if ( result === null ) {
        $state.go(divConst.moduleState);
    } else {
        for(var key in result) {
            result[key].runeImage   = imgPath + oneRuneFactory.getResultRuneImg(result[key]);
            result[key].runeState   = oneRuneFactory.getResultRuneState(result[key]);
            result[key].runeName          = oneRuneFactory.getResultRuneName(result[key]);
            result[key].resultDescription = oneRuneFactory.getResultDescription(result[key]);
        }

        _self.result = result;
        _self.currentDivination = divFactory.getCurrentDivination();
    }

    return $scope.DivinationResultCtrl = _self;
}])

//*******************************************************************
.factory('divinationFactory', ['$http', 'DivinationConst', 'appTranslation',
function($http, divinationConst, appTranslation) {
    "use strict";
    var _self = this;

    return {
        /**
         * Stores the available divinations
         */
        divinations: [],

        /**
         * Store the question that was asked
         */
        question: '',

        /**
         * Stores the current divination ID
         */
        currentDivination: null,

        /**
         * The result of server request for divination.
         * It could be one rune object or several, depending on divination type
         */
        result: null,

        /**
         * true if some error has occured previously
         */
        generalError: null,

        /**
         * Load divinations from the server.
         *
         * @returns deferred object
         */
        getAllDivinations: function() {
            return $http.get(divinationConst.listAllUrl)
            .success(function(data, status, headers, config) {
            })
            .error(function(data, status, headers, config) {
                this.generalError = true;
                alert('Error getting list of divinations.');
            });
        },

        /**
         * Set divinations by received value.
         *
         * @param divinations
         * @returns divinationFactory
         */
        setDivinations: function(divinations) {
            var _self = this;

            _self.divinations = divinations;

            return _self;
        },

        /**
         * Returns the current value of divinations array.
         *
         * @returns Array
         */
        getDivinations: function() {
            return this.divinations;
        },

        /**
         * Applies translations to the divinations according to the current locale.
         *
         * @returns divinationFactory
         */
        applyTranslationsToDivinations: function() {
            var _self = this;

            for(var i=0; i < _self.divinations.length; i++) {
                _self.divinations[i].name = appTranslation.getString(_self.divinations[i].name_key, appTranslation.translatedStrings);
                _self.divinations[i].description = appTranslation.getString(_self.divinations[i].description_key, appTranslation.translatedStrings);
            }

            return _self;
        },

        /**
         * Returns one divination by its ID.
         *
         * @param divinationId
         * @returns Object
         */
        getOneDivination: function(divinationId) {
            var _self = this;
            var divinations = _self.divinations;
            var divination = null;
            for(var i=0; i < divinations.length; i++) {
                if ( divinations[i].id === parseInt(divinationId, 10) ) {
                    divination = divinations[i];
                    break;
                }
            }

            return divination;
        },

        /**
         * Sets the question which is being asked
         *
         * @param String question
         * @returns divinationFactory
         */
        setQuestion: function(question) {
            var _self = this;

            _self.question = question;

            return _self;
        },

        /**
         * Returns the current value of a question.
         *
         * @returns String
         */
        getQuestion: function() {
            return this.question;
        },

        /**
         * Sets the current divination property.
         * @param divination
         * @returns divinationFactory
         */
        setCurrentDivination: function(divination) {
            this.currentDivination = divination;

            return this;
        },

        /**
         * Returns the current divination
         *
         * @returns Object the current divination
         */
        getCurrentDivination: function() {
            return this.currentDivination;
        },

        /**
         * Returns the result of divination from a server.
         *
         * @returns Object deferred object promise
         */
        getResult: function() {
            var question = this.getQuestion();
            var divinationId = this.getCurrentDivination().id;

            return $http.post(divinationConst.getAnswerUrl,
                {
                    question:       question,
                    divinationId:   divinationId
                }
            )
            .success(function(data, status, headers, config) {
            })
            .error(function(data, status, headers, config) {

            });
        },

        /**
         * Translates the result of divination (request to server)
         *
         * @return divinationFactory
         */
        translateResult: function() {
            var _self = this;

            var ucfirst = appTranslation.ucfirst;

            var qtyRunes = 1;
            if ( _self.result.length !== undefined ) {
                qtyRunes = _self.result.length;
            }

            var i = 0;
            do {
                _self.result[i].time = (qtyRunes > 1) ?
                    ucfirst(appTranslation.getString(_self.result[i].timePeriod)) : '';

                _self.result[i].name = ucfirst(appTranslation.getString(_self.result[i].name_key));
                _self.result[i].directDescription = appTranslation.getString(_self.result[i].direct_description_key);
                _self.result[i].indirectDescription = appTranslation.getString(_self.result[i].indirect_description_key);

                i++;
            } while(i < qtyRunes);

            return _self;
        },

        /**
         * Return the name of state for the current divination result
         * @returns String
         */
        getCurrentDivinationState: function() {
            for(var i in divinationConst.divinationTypes) {
                var divination = divinationConst.divinationTypes[i];

                if ( divination.id === parseInt(this.getCurrentDivination().id, 10) ) {
                    return divination.state;
                }
            }

            return '';
        }
    };
}])

//*******************************************************************
.factory('OneRuneDivinationFactory', ['appTranslation', function(appTranslation) {
    return {
        /**
         * return the image of a rune depending on the divination result
         *
         * @returns String
         */
        getResultRuneImg: function(rune) {
            var retResult = null;

            if ( rune.isDirect ) {
                retResult = rune.direct_img;
            } else {
                retResult = rune.indirect_img;
            }

            return retResult;
        },

        /**
         *
         * @param Object
         * @returns String
         */
        getResultRuneName: function(rune) {
            return rune.name;
        },

        /**
         *
         * @param Object
         * @returns String
         */
        getResultDescription: function(rune) {
            var description =  null;

            if ( rune.isDirect ) {
                description = rune.directDescription;
            } else {
                description = rune.indirectDescription;
            }

            return description;
        },

        /**
         *
         * @param Object
         * @returns String
         */
        getResultRuneState: function(rune) {
            var runeState = null;

            var ucfirst = appTranslation.ucfirst;

            if ( rune.isDirect ) {
                runeState = ucfirst(appTranslation.getString('direct_position'));
            } else {
                runeState = ucfirst(appTranslation.getString('reversed_position'));
            }

            return runeState;
        }
    };
}])

//*******************************************************************
.constant('DivinationConst', {
    'baseStateName':    'appState',

    'moduleName':           'divination',
    'moduleState':          'appState.divination',
    'moduleUrl':            '/divination',

    'divinationDescState':      'appState.divination.description',
    'divinationDescUrl':        '/description/{divinationId}',
    'divinationDescTitle':      'Divination Description',
    'divinationDescController': 'DivinationDescCtrl',
    'divinationDescTplName':    'divination-description',

    'listController':           'DivinationListCtrl',
    'listPageTitle':            'List of Divinations',
    'listTplName':              'divination-list',
    'listAllUrl':               '/divination/all',

    'imagesPath':   '/images/runes/',

    divinationTypes:    [
        {
            id:         1,
            controller: 'DivinationResultCtrl',
            state:      'appState.DivinationResult',
            url:        '/divination-result',
            templateUrl:'divination-result',
            pageTitle:  'Divination Result'
        },
        {
            id:         2,
            state:      'appState.DivinationResult'

        }
    ],

    'getAnswerUrl':     '/result/get'
})

;