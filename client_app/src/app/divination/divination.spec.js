describe('Testing app.divination module, DivinationResultCtrl for One Rune Divination', function() {
    var $rootScope, $scope, createCtrl, result, mockDivFactory;

    result = [
        {
            name:                   'runeName',
            isDirect:               true,
            direct_img:             'runedirect.png',
            indirect_img:           'runereverse.png',
            directDescription:      'direct_description',
            indirectDescription:    'indirect_description'
        }
    ];

    beforeEach(module('app.divination'));

    beforeEach(inject(function($injector){
        $rootScope  = $injector.get('$rootScope');
        $scope      = $rootScope.$new();

        var $controller = $injector.get('$controller');

        mockDivFactory = {
            result:                 result,
            getCurrentDivination:   function() {
                return {};
            }
        };

        createCtrl = function() {
            var ct = $controller('DivinationResultCtrl', {
                '$scope':               $scope,
                'divinationFactory':    mockDivFactory
            });

            return ct;
        };
    }));

    it('test DivinationResultCtrl for One Rune Divination', function() {
        var ctrl = createCtrl();

        expect(ctrl).toBeTruthy();
        expect(ctrl.result[0].runeName).toBe('runeName');
        expect(ctrl.result[0].runeState).toBe('Direct_position');
        expect(ctrl.result[0].runeImage).toBe('/images/runes/runedirect.png');
        expect(ctrl.result[0].resultDescription).toBe('direct_description');
    });
});