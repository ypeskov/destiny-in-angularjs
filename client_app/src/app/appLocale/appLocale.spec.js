describe('Test appTranslation factory', function() {
    "use strict";

    var appTranslation, httpBackend, $locale = {};

    beforeEach(module('app.appLocale'));

    beforeEach(inject(function(_appTranslation_, $httpBackend, _$locale_) {
        $locale = _$locale_;
        appTranslation  = _appTranslation_;
        httpBackend     = $httpBackend;
    }));

    it('Test that translated strings are assigned to appTranslation factory', function() {
        //test en-us locale requests. For rest of locales it is the same
        $locale.id = 'en-us';

        httpBackend
            .whenGET('/locale/all?locale=en-us')
            .respond({
                is_translated: true
        });

        appTranslation.loadStrings()
        .then(function() {
            expect(appTranslation.translatedStrings.is_translated).toBeTruthy();
        });

        httpBackend.flush();
    });

    it('Test the case if server doesn\'t translate strings', function() {
        //test en-us locale requests. For rest of locales it is the same
        $locale.id = 'en-us';

        httpBackend
        .whenGET('/locale/all?locale=en-us')
        .respond({
            is_translated: false
        });

        appTranslation.loadStrings()
        .then(function() {
            expect(appTranslation.generalError).toBeTruthy();
        });

        httpBackend.flush();
    });

    it('Test error response from the server', function() {
        //non-existing fake locale
        $locale.id = 'qqq';

        httpBackend
        .whenGET('/locale/all?locale=qqq')
        .respond(500, null);

        appTranslation.loadStrings()
        .then(function() {

        }, function() {
            expect(appTranslation.generalError).toBeTruthy();
        });

        httpBackend.flush();
    });

    it('Test appTranslation.getString(). If key doesn\'t exist return parameter itself',
    function() {
        appTranslation.translatedStrings['testString'] = 'translated test string';

        var translation = appTranslation.getString('testString');
        expect(translation).toBe('translated test string');

        translation = appTranslation.getString('nonExistingKey');
        expect(translation).toBe('nonExistingKey');
    });

    it('Test appTranslation.ucfirst()', function() {
        expect(appTranslation.ucfirst('test')).toBe('Test');
    });

    it('Test that getStrings() returns the correct number of strings', function() {
        appTranslation.translatedStrings['testString1'] = 'translated test string1';
        appTranslation.translatedStrings['testString2'] = 'translated test string2';
        appTranslation.translatedStrings['testString3'] = 'translated test string3';

        var i = 0;
        for(var key in appTranslation.getStrings()) {
            i++;
        }

        expect(appTranslation.getStrings().testString2).toBe('translated test string2');
        expect(i).toBe(3);
    });
});