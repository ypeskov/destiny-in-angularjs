describe( 'Test that Home Controller is created OK', function() {
    var controller, $scope;

    beforeEach( module( 'app.home' ) );

    beforeEach(inject(function($controller, $rootScope) {
        $scope      = $rootScope.$new();
        controller  = $controller('HomeCtrl', {
            $scope:     $scope
        });

    }));

    it( 'Test that HomeCtrl controller is available', inject( function() {
        expect(controller).toBeTruthy();
        expect($scope.HomeCtrl).toBeTruthy();
    }));
});

